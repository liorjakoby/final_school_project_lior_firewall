import tkinter as tk
from tkinter import ttk
from tkinter import *
import scapy.all as scapy
import threading
import collections
import json
import socket
from os import path
import ipaddress

is_sniffing = True
global src_ip_dict

treevIp = None
rowIp = None
cntIp = 0
treevNet = None
rowNet = None
cntNet = 0
treevPort = None
rowPort = None
cntPort = 0
treevUrl = None
rowUrl = None
cntUrl = 0


filePath = "C://Users//user//Desktop//lior//firewallrules.json"


def start_button():
    print('Start button clicked')
    global should_we_stop
    global thread
    global subnet

    subnet = subnet_entry.get()

    if(thread is None) or (not thread.is_alive()):
        should_we_stop = False
        thread = threading.Thread(target=sniffing)
        thread.start()


def stop_button():
    print('Stop button clicked')
    global should_we_stop
    should_we_stop = True


def sniffing():
    scapy.sniff(prn=find_ips, stop_filter=stop_sniffing)


def stop_sniffing(packet):
    global should_we_stop
    return should_we_stop


def submit_Ip_button():
    global getIP, getIpString, cntIp, treevIp, rowIp
    ipListAddresses = []
    getIp = ip_entry.get()
    if getIp != "":
        with open(filePath, "r") as file:
            getIpString = json.load(file)
            if getIp not in getIpString["black_list"][0]["ip"]:
                (getIpString["black_list"][0])["ip"].append(getIp)
                # Verify updated list
                with open(filePath, "w") as file:
                    json.dump(getIpString, file)
                for ip in (getIpString["black_list"][0])["ip"]:
                    ipListAddresses.append(ip + ' ')

        if not treevIp:
            treevIp = ttk.Treeview(height=2)
            treevIp.column('#0')

        if cntIp == 0:
            tk.Label(text="***Blocked Ip's***", font="Helvetica 7 bold").pack()
            rowIp = treevIp.insert('', index=tk.END, text=f"ip")

        treevIp.insert(rowIp, tk.END, text=ipListAddresses)
        treevIp.pack(fill=tk.X)

    cntIp = cntIp + 1


def remove_Ip_button():
    global getIP, getIpString, treevIp, rowIp
    ipListAddresses = []
    getIp = ip_entry.get()
    if getIp != "":
        with open(filePath, "r") as file:
            getIpString = json.load(file)
            if getIp in getIpString["black_list"][0]["ip"]:
                (getIpString["black_list"][0])["ip"].remove(getIp)
                # Verify updated list
                with open(filePath, "w") as file:
                    json.dump(getIpString, file)
                for ip in (getIpString["black_list"][0])["ip"]:
                    ipListAddresses.append(ip + ' ')

        treevIp.insert(rowIp, tk.END, text=ipListAddresses)
        treevIp.pack(fill=tk.X)



def submit_Net_button():
    global getNet, getNetString, cntNet, treevNet, rowNet
    netListAddresses = []
    getNet = net_entry.get()
    if getNet != "":
        with open(filePath, "r") as file:
            getNetString = json.load(file)
            if getNet not in getNetString["black_list"][1]["net"]:
                (getNetString["black_list"][1])["net"].append(getNet)
                with open(filePath, "w") as file:
                    json.dump(getNetString, file)

                for net in (getNetString["black_list"][1])["net"]:
                    netListAddresses.append(net + ' ')

        if not treevNet:
            treevNet = ttk.Treeview(height=2)
            treevNet.column('#0')

        if cntNet == 0:
            tk.Label(text="***Blocked Net's***", font="Helvetica 7 bold").pack()
            rowNet = treevNet.insert('', index=tk.END, text=f"net")

        treevNet.insert(rowNet, tk.END, text=netListAddresses)
        treevNet.pack(fill=tk.X)

    cntNet = cntNet + 1


def remove_Net_button():
    global getNet, getNetString, treevNet, rowNet
    netListAddresses = []
    getNet = net_entry.get()
    if getNet != "":
        with open(filePath, "r") as file:
            getNetString = json.load(file)
            if getNet in getNetString["black_list"][1]["net"]:
                (getNetString["black_list"][1])["net"].remove(getNet)
                with open(filePath, "w") as file:
                    json.dump(getNetString, file)

                for net in (getNetString["black_list"][1])["net"]:
                    netListAddresses.append(net + ' ')

        treevNet.insert(rowNet, tk.END, text=netListAddresses)
        treevNet.pack(fill=tk.X)


def submit_Port_button():
    global getPort, getPortString, cntPort ,treevPort, rowPort
    portListAddresses = []
    getPort = port_entry.get()
    if getPort != "":
        with open(filePath, "r") as file:
            getPortString = json.load(file)
            if getPort not in getPortString["black_list"][2]["port"]:
                (getPortString["black_list"][2])["port"].append(getPort)
                # Verify updated list
                with open(filePath, "w") as file:
                    json.dump(getPortString, file)

                for port in (getPortString["black_list"][2])["port"]:
                    portListAddresses.append(port + ' ')

        if not treevPort:
            treevPort = ttk.Treeview(height=2)
            treevPort.column('#0')

        if cntPort == 0:
            tk.Label(text="***Blocked Port's***", font="Helvetica 7 bold").pack()
            rowPort = treevPort.insert('', index=tk.END, text=f"port")

        treevPort.insert(rowPort, tk.END, text=portListAddresses)
        treevPort.pack(fill=tk.X)

    cntPort = cntPort + 1


def remove_Port_button():
    global getPort, getPortString, treevPort, rowPort
    portListAddresses = []
    getPort = port_entry.get()
    if getPort != "":
        with open(filePath, "r") as file:
            getPortString = json.load(file)
            if getPort in getPortString["black_list"][2]["port"]:
                (getPortString["black_list"][2])["port"].remove(getPort)
                # Verify updated list
                with open(filePath, "w") as file:
                    json.dump(getPortString, file)

                for port in (getPortString["black_list"][2])["port"]:
                    portListAddresses.append(port + ' ')

        treevPort.insert(rowPort, tk.END, text=portListAddresses)
        treevPort.pack(fill=tk.X)


def submit_URL_button():
    host_path ='C://Windows//System32//drivers//etc//hosts'
    ip_address = '127.0.0.1'
    global getURL, getURLString, treevUrl, rowUrl, cntUrl
    urlListAddresses = []
    getURL = url_entry.get()

    if getURL != "":
        with open(filePath, "r") as file:
            getURLString = json.load(file)
            if getURL not in getURLString["black_list"][3]["URL"]:
                (getURLString["black_list"][3])["URL"].append(getURL)
                # Verify updated list
                with open(filePath, "w") as file:
                    json.dump(getURLString, file)

                with open(host_path, 'r+') as host_file:
                    host_file.readlines()
                    host_file.write('\n' + ip_address + " " + getURL + '\n')
                    host_file.write('\n' + ip_address + " www." + getURL + '\n')

                for url in (getURLString["black_list"][3])["URL"]:
                    urlListAddresses.append(url + ' ')

        if not treevUrl:
            treevUrl = ttk.Treeview(height=2)
            treevUrl.column('#0')

        if cntUrl == 0:
            tk.Label(text="***Blocked URL's***", font="Helvetica 7 bold").pack()
            rowUrl = treevUrl.insert('', index=tk.END, text=f"url")

        treevUrl.insert(rowUrl, tk.END, text=urlListAddresses)
        treevUrl.pack(fill=tk.X)
    cntUrl = cntUrl + 1


def remove_url_button():
    global getURL, getURLString, treevUrl, rowUrl
    urlListAddresses = []
    host_path ='C://Windows//System32//drivers//etc//hosts'
    ip_address = '127.0.0.1'
    getURL = url_entry.get()
    if getURL != "":
        with open(filePath, "r") as file:
            getURLString = json.load(file)
            if getURL in getURLString["black_list"][3]["URL"]:
                (getURLString["black_list"][3])["URL"].remove(getURL)
                # Verify updated list
                with open(filePath, "w") as file:
                    json.dump(getURLString, file)

        hostFile = open(host_path,'r')
        address = ['www.'+ getURL, getURL, ip_address]
        lst = []
        for line in hostFile:
            for word in address:
                if word in line:
                    line = line.replace(word,'')
            lst.append(line)
        hostFile.close()
        hostFile = open(host_path,'w')
        for line in lst:
            hostFile.write(line)
        hostFile.close()

        for url in (getURLString["black_list"][3])["URL"]:
            urlListAddresses.append(url + ' ')

        treevUrl.insert(rowUrl, tk.END, text=urlListAddresses)
        treevUrl.pack(fill=tk.X)


def submit_All_button():
    # ip
    submit_Ip_button()
    # net
    submit_Net_button()
    # port
    submit_Port_button()
    # URL
    submit_URL_button()


def find_ips(packet):
    global src_ip_dict
    global treev
    global subnet
    global src_ip
    global dst_ip

    # print(packet.show())

    if 'IP' in packet:

        src_ip = packet['IP'].src
        dst_ip = packet['IP'].dst

        # blocked_ip(src_ip, dst_ip)

        if src_ip[0:len(subnet)] == subnet:
            if src_ip not in src_ip_dict:
                src_ip_dict[src_ip].append(dst_ip)

                row = treev.insert('', index=tk.END, text=src_ip)
                treev.insert(row, tk.END, text=dst_ip)
                treev.pack(fill=tk.X)

            else:
                if dst_ip not in src_ip_dict[src_ip]:
                    src_ip_dict[src_ip].append(dst_ip)
                    cur_item = treev.focus()
                    if treev.item(cur_item)['text'] == src_ip:
                        treev.insert(cur_item, tk.END, text=dst_ip)



def get_blocked_ip(file_name):
    global src_ip_dict
    final_blocked_ip = []
    with open(file_name, "r") as file:
        rule_file = json.loads(file.read())

        # Check for IP to block
        for key, value in src_ip_dict.items():
            for ips in rule_file["black_list"][0].values():
                for ip in ips:
                    if ip in key:
                        final_blocked_ip.append(ip)
        #print(final_blocked_ip)
        return final_blocked_ip


def get_blocked_net(file_name):
    final_blocked_net = []
    global src_ip
    global dst_ip

    with open(file_name, "r") as file:
        rule_file = json.loads(file.read())

    # Check for NET and block all the IP in it
    for net_list in rule_file["black_list"][1].values():
        for net in net_list:
            if ipaddress.IPv4Address(src_ip) in ipaddress.IPv4Network(net):
                final_blocked_net.append(src_ip)
                print('Net', net, 'was blocked...!!!')

            elif ipaddress.IPv4Address(dst_ip) in ipaddress.IPv4Network(net):
                final_blocked_net.append(dst_ip)
                print('Net', net, 'was blocked...!!!')

    return final_blocked_net


def get_allowed_port(file_name):
    final_allow_port = []
    with open(file_name, "r") as file:
        rule_file = json.loads(file.read())

    for port in str(rule_file["black_list"][2].values()):
        final_allow_port.append(port)

    return final_allow_port


def start_GUI():
    global thread, subnet_entry, thread2, should_we_stop, subnet, src_ip_dict, treev, treevURL, rowUrl, ip_entry, net_entry, port_entry, url_entry
    thread = None
    thread2 = None
    should_we_stop = True
    subnet = ''

    src_ip_dict = collections.defaultdict(list)

    root = tk.Tk()
    root.geometry('720x700')
    root.title('Firewall Analyzer')

# sniffing line
    tk.Label(root, text='Firewall Sniffer', font="Helvetica 9 bold").pack()
    tk.Label(root, text="Enter an IP Subnet", font="Helvetica 7 bold").pack()
    subnet_entry = tk.Entry(root)
    subnet_entry.pack(ipady=2, ipadx=2, pady=2)

# Blocking IP
    tk.Label(root, text="Enter an IP address", font="Helvetica 7 bold").pack()
    ip_entry = tk.Entry(root)
    ip_entry.pack(ipady=2, ipadx=2, pady=2)

# Blocking NET
    tk.Label(root, text="Enter a NET address", font="Helvetica 7 bold").pack()
    net_entry = tk.Entry(root)
    net_entry.pack(ipady=2, ipadx=2, pady=2)

# Allow PORT
    tk.Label(root, text="Enter a PORT number", font="Helvetica 7 bold").pack()
    port_entry = tk.Entry(root)
    port_entry.pack(ipady=2, ipadx=2, pady=2)

# Blocking URL
    tk.Label(root, text="Enter URL Address", font="Helvetica 7 bold").pack()
    url_entry = tk.Entry(root)
    url_entry.pack(ipady=2, ipadx=2, pady=2)

    # URL treev
    treevURL = ttk.Treeview(height=2)
    treevURL.column('#0')

    # Port treev
    treevPort = ttk.Treeview(root, height=2)
    treevPort.column('#0')

    # Net treev
    treevNet = ttk.Treeview(root, height=2)
    treevNet.column('#0')

    # Ip treev
    treevIp = ttk.Treeview(root, height=2)
    treevIp.column('#0')

# Ipfilter treev
    treev = ttk.Treeview(root, height=2)
    treev.column('#0')

    button_frame = tk.Frame(root)

    tk.Button(button_frame, text='Start Sniffing', fg="green", command=start_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    tk.Button(button_frame, text='Stop Sniffing', fg="red", command=stop_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # submit_IP button
    tk.Button(button_frame, text='Submit Ip', fg="green", command=submit_Ip_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # remove_IP button
    tk.Button(button_frame, text='Remove Ip', fg="red", command=remove_Ip_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # submit_NET button
    tk.Button(button_frame, text='Submit Net', fg="green", command=submit_Net_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # remove_NET button
    tk.Button(button_frame, text='Remove Net', fg="red", command=remove_Net_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # submit_PORT button
    tk.Button(button_frame, text='Submit Port', fg="green", command=submit_Port_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # remove_PORT button
    tk.Button(button_frame, text='Remove Port', fg="red", command=remove_Port_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # URL button
    tk.Button(button_frame, text='Submit URL', fg="green", command=submit_URL_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # remove_URL button
    tk.Button(button_frame, text='Remove URL', fg="red", command=remove_url_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=3)
    # submit_ALL button
    tk.Button(button_frame, text='Submit All', fg="green", command=submit_All_button, width=8, font="Helvetica 7 bold").pack(side=tk.LEFT)
    button_frame.pack(side=tk.BOTTOM, pady=5)



    root.mainloop()
