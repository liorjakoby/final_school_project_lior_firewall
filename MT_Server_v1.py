import tkinter as tk
from tkinter import ttk
import collections
import socket
import threading
from SniffAndBlock_v1 import start_button, stop_button, submit_Ip_button, submit_Net_button, submit_Port_button, \
    submit_URL_button, sniffing, stop_sniffing, find_ips, get_blocked_ip, get_blocked_net, get_allowed_port, \
    start_GUI

blocked = collections.defaultdict(list)


class ClientThread(threading.Thread):
    def __init__(self, clientAddress, clientsocket):
        threading.Thread.__init__(self)
        self.csocket = clientsocket
        print("New connection added: ", clientAddress)

    def run(self):
        print("Connection from : ", clientAddress)
        self.csocket.send(bytes("Hi, This is from Server..", 'utf-8'))
        msg = ''
        while True:
            data = self.csocket.recv(2048)
            msg = data.decode()
            if msg == 'bye':
                break
            print("from client", msg)
            self.csocket.send(bytes(msg,'UTF-8'))
        print("Client at ", clientAddress, " disconnected...")


LOCALHOST = "0.0.0.0"
PORT = 8080

trd = threading.Thread(target=start_GUI)
trd.start()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((LOCALHOST, PORT))
print("Server started")
print("Waiting for client request..")

while True:
    server.listen(1)
    clientsock, clientAddress = server.accept()
    blocking_ip = get_blocked_ip("C://Users//user//Desktop//lior//firewallrules.json")
    blocking_net = get_blocked_net("C://Users//user//Desktop//lior//firewallrules.json")
    allowed_ports = get_allowed_port("C://Users//user//Desktop//lior//firewallrules.json")


    if clientAddress[0] in blocking_ip or clientAddress[0] in blocking_net or clientAddress[1] not in allowed_ports:
        print(clientAddress, "-> Client Blocked...!!!")

    else:
        newthread = ClientThread(clientAddress, clientsock)
        newthread.start()
